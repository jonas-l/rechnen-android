/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.ui.util

import android.content.Context
import com.rechnen.app.R

object Time {
    private fun formatMinutes(minutes: Int, context: Context) = context.resources.getQuantityString(R.plurals.time_minute, minutes, minutes)
    private fun formatHours(hours: Int, context: Context) = context.resources.getQuantityString(R.plurals.time_hour, hours, hours)
    private fun and(a: String, b: String, context: Context) = context.getString(R.string.time_and, a, b)

    fun formatTime(timeInMillis: Long, context: Context): String {
        val totalMinutes = timeInMillis / (1000 * 60)

        return if (totalMinutes <= 0L) {
            context.getString(R.string.time_less_than_one_minute)
        } else {
            val hours = (totalMinutes / 60).toInt()
            val minutes = (totalMinutes % 60).toInt()

            if (hours == 0) {
                formatMinutes(minutes, context)
            } else if (minutes == 0) {
                formatHours(hours, context)
            } else {
                and(
                        formatHours(hours, context),
                        formatHours(hours, context),
                        context
                )
            }
        }
    }
}