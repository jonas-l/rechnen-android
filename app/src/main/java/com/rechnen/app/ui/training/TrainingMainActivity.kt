/*
 * Calculate Android Copyright <C> 2020 - 2022 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.ui.training

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.*
import android.widget.RelativeLayout
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager

import com.rechnen.app.R
import com.rechnen.app.Threads
import com.rechnen.app.data.AppDatabase
import com.rechnen.app.data.modelparts.InputConfiguration
import com.rechnen.app.databinding.ActivityTrainingMainBinding
import com.rechnen.app.ui.training.statistic.TrainingStatisticAdapter
import com.rechnen.app.ui.training.statistic.TrainingStatisticListItem
import com.rechnen.app.ui.view.KeyboardView

class TrainingMainActivity : FragmentActivity() {
    companion object {
        private const val EXTRA_USER_ID = "userId"
        private const val EXTRA_MODE = "mode"

        private const val PAGE_TRAINING = 0
        private const val PAGE_RESULTS = 1
        private const val PAGE_COUNTDOWN = 2

        fun launch(context: Context, userId: Int, mode: TrainingMode) {
            context.startActivity(
                    Intent(context, TrainingMainActivity::class.java)
                            .putExtra(EXTRA_USER_ID, userId)
                            .putExtra(EXTRA_MODE, mode)
            )
        }

        fun applyConfigToKeyboard(config: InputConfiguration, keyboard: KeyboardView) {
            keyboard.type = config.type
            keyboard.scaleFactor = config.size
            keyboard.confirmButtonLocation = config.confirmButtonLocation

            keyboard.layoutParams = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT).also { params ->
                when (config.vertical) {
                    InputConfiguration.VerticalAlignment.Top -> params.addRule(RelativeLayout.ALIGN_PARENT_TOP)
                    InputConfiguration.VerticalAlignment.Center -> params.addRule(RelativeLayout.CENTER_VERTICAL)
                    InputConfiguration.VerticalAlignment.Bottom -> params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
                }.let {/* require handling all paths */}

                when (config.horizontal) {
                    InputConfiguration.HorizontalAlignment.Left -> params.addRule(RelativeLayout.ALIGN_PARENT_LEFT)
                    InputConfiguration.HorizontalAlignment.Center -> params.addRule(RelativeLayout.CENTER_HORIZONTAL)
                    InputConfiguration.HorizontalAlignment.Right -> params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT)
                }.let {/* require handling all paths */}
            }

        }
    }

    private val model: TrainingModel by viewModels()

    private lateinit var playtimeSaver: PlaytimeSaver

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val database = AppDatabase.with(this)
        val binding = DataBindingUtil.setContentView<ActivityTrainingMainBinding>(this, R.layout.activity_training_main)
        val userId = intent.extras!!.getInt(EXTRA_USER_ID)
        val mode = intent.extras!!.getSerializable(EXTRA_MODE) as TrainingMode
        val statisticAdapter = TrainingStatisticAdapter()
        val layoutManager = LinearLayoutManager(this)

        playtimeSaver = PlaytimeSaver(userId, database)

        model.init(
                database = database,
                userId = userId,
                mode = mode
        )

        model.state.observe(this) { state ->
            val trainingState = state.traning
            val inputState = state.input

            when (trainingState) {
                is TrainingState.ShowTask -> {
                    playtimeSaver.startOrContinue()

                    binding.flipper.displayedChild = PAGE_TRAINING

                    binding.task.lines = trainingState.task.taskPresentation.let { presentation ->
                        val resultLine = TaskView.Line(left = "=", right = inputState.toString(), ellipsize = "...")
                        val emptyLine = TaskView.Line(left = "", right = "")

                        if (presentation.size == 1)
                            TaskView.Lines(presentation + listOf(resultLine, emptyLine))
                        else
                            TaskView.Lines(presentation + listOf(resultLine))
                    }

                    when (trainingState) {
                        is TrainingState.ShowTask.RegularModeTask -> binding.progress.apply {
                            progress = trainingState.currentQuestionIndex + 1
                            max = trainingState.totalQuestionCounter
                        }
                        is TrainingState.ShowTask.TimeTrailTask -> binding.progress.apply {
                            progress = (trainingState.elapsedTime * 1000L / trainingState.totalTime).toInt()
                            max = 1000
                        }
                    }.let {/* require handling all cases */}

                    binding.keyboard.isEnabled = trainingState.state == TrainingShowTaskMode.WaitingForInput

                    binding.keyboard.confirmButtonMode = when (trainingState.state) {
                        TrainingShowTaskMode.FeedbackRight -> KeyboardView.ConfirmButton.ShowRight
                        TrainingShowTaskMode.FeedbackWrong -> KeyboardView.ConfirmButton.ShowWrong
                        TrainingShowTaskMode.WaitingForInput -> when (inputState.specialInputType) {
                            SpecialInputType.Nothing -> KeyboardView.ConfirmButton.Regular
                            SpecialInputType.NextItem -> KeyboardView.ConfirmButton.Special(trainingState.task.separator!!)
                            SpecialInputType.TogglePrefix -> KeyboardView.ConfirmButton.Special(trainingState.task.prefix!!)
                        }
                    }

                    statisticAdapter.items = emptyList()

                    applyConfigToKeyboard(trainingState.inputConfiguration, binding.keyboard)

                    null
                }
                is TrainingState.EndOfRound -> {
                    playtimeSaver.stop()

                    binding.flipper.displayedChild = PAGE_RESULTS

                    when (trainingState) {
                        is TrainingState.EndOfRound.RegularEndOfRound -> {
                            binding.resultScreen.wasGood = trainingState.result == TrainingRoundEndState.EverythingGood

                            statisticAdapter.items = listOf(
                                    TrainingStatisticListItem.ResultInfoHeadline {
                                        when (trainingState.result) {
                                            TrainingRoundEndState.EverythingGood -> if (trainingState.mistakes == 0)
                                                it.getString(R.string.training_block_result_perfect)
                                            else
                                                it.resources.getQuantityString(R.plurals.training_block_result_good, trainingState.mistakes, trainingState.mistakes)
                                            TrainingRoundEndState.TooMuchMistakes -> it.resources.getQuantityString(R.plurals.training_block_result_too_much_mistakes, trainingState.mistakes, trainingState.mistakes)
                                            TrainingRoundEndState.TooSlow -> it.resources.getString(R.string.training_block_result_too_slow, formatMistakes(trainingState.mistakes), formatSeconds((trainingState.time / 1000).toInt()))
                                        }
                                    }
                            ) + TrainingStatisticListItem.from(trainingState.statistic) + listOf(TrainingStatisticListItem.ResultFooterText(R.string.training_result_difficulty_tip))
                        }
                        is TrainingState.EndOfRound.TimeTrailEndOfRound -> {
                            binding.resultScreen.wasGood = true

                            val totalTasks = trainingState.tasks.size
                            val correctTasks = trainingState.tasks.filter { it.correct }.size

                            statisticAdapter.items = listOf(
                                    TrainingStatisticListItem.ResultInfoHeadline {
                                        it.getString(
                                                R.string.time_trial_result_headline,
                                                it.resources.getQuantityString(R.plurals.difficulty_general_summary_tasks, totalTasks, totalTasks),
                                                it.resources.getQuantityString(R.plurals.difficulty_general_summary_tasks, correctTasks, correctTasks)
                                        )
                                    }
                            ) + trainingState.tasks.map {
                                TrainingStatisticListItem.TaskInfo(
                                        task = it.task.readableTask,
                                        correct = it.correct,
                                        duration = it.time,
                                        input = it.input.format(it.task)
                                ) as TrainingStatisticListItem
                            }
                        }
                    }.let {/* require handling all paths */}

                    binding.resultScreen.recycler.let { recycler ->
                        recycler.visibility = View.INVISIBLE
                        layoutManager.scrollToPositionWithOffset(0, 0)

                        Threads.mainThreadHandler.post {
                            recycler.scrollBy(0, resources.displayMetrics.densityDpi / 2)
                            recycler.smoothScrollBy(0, -resources.displayMetrics.densityDpi / 2, DecelerateInterpolator(), 1000)

                            recycler.visibility = View.VISIBLE
                        }
                    }
                }
                is TrainingState.Countdown -> {
                    playtimeSaver.stop()

                    binding.flipper.displayedChild = PAGE_COUNTDOWN

                    binding.countdown.countdown = trainingState.remainingSeconds.toString()
                    binding.countdown.info = trainingState.info

                    null
                }
            }.let {/* require handling all cases */}
        }

        binding.keyboard.listener = object: KeyboardView.Listener {
            override fun onDigitEntered(digit: Int) { playtimeSaver.startOrContinue(); model.handleNumberButton(digit) }
            override fun onConfirmClicked() { playtimeSaver.startOrContinue(); model.handleConfirmButton() }
            override fun onDeleteClicked() { playtimeSaver.startOrContinue(); model.handleDeleteButton() }
        }

        binding.resultScreen.continueButton.setOnClickListener { playtimeSaver.startOrContinue(); model.handleContinueButton() }

        binding.resultScreen.recycler.let { recycler ->
            recycler.layoutManager = layoutManager
            recycler.adapter = statisticAdapter
        }
    }

    override fun onStop() {
        super.onStop()

        playtimeSaver.stop()
    }

    private fun formatMistakes(mistakes: Int) = resources.getQuantityString(R.plurals.training_result_mistakes, mistakes, mistakes)
    private fun formatSeconds(seconds: Int) = resources.getQuantityString(R.plurals.training_result_seconds, seconds, seconds)
}
