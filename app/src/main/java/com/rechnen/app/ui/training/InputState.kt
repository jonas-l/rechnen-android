/*
 * Calculate Android Copyright <C> 2020 - 2022 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */

package com.rechnen.app.ui.training

data class InputState(
        val maxListLength: Int,
        val prefix: String?,
        val separator: String?,
        val previousItems: List<Long>,
        val prefixSet: Boolean,
        val currentNumberEncoded: Long
) {
    companion object {
        private const val BASE = 10

        fun newInstance(prefix: String?, separator: String?, maxListLength: Int) = InputState(
                maxListLength = maxListLength,
                prefix = prefix,
                separator = separator,
                prefixSet = false,
                previousItems = emptyList(),
                currentNumberEncoded = 0
        )

        private fun decodeNumber(number: Long): Long {
            var input = number
            var result = 0L
            var nextFactor = 1L

            while (input > 0) {
                result += (input % (BASE + 1) - 1) * nextFactor
                nextFactor *= BASE
                input /= (BASE + 1)
            }

            return result
        }

        private fun stringifyEncodedNumber(number: Long): String {
            var input = number
            val result = StringBuilder()

            while (input > 0) {
                result.append(('0'.toInt() + input % (BASE + 1) - 1).toChar())
                input /= (BASE + 1)
            }

            return result.reverse().toString()
        }
    }

    init {
        if (maxListLength < 1) throw IllegalArgumentException()
        if (maxListLength > 1 && separator == null) throw IllegalArgumentException()
        if (currentNumberEncoded < 0) throw IllegalArgumentException()
    }

    val specialInputType: SpecialInputType = kotlin.run {
        val isStart = previousItems.isEmpty() && currentNumberEncoded == 0L
        val isLastItem = previousItems.size + 1 == maxListLength

        if (isStart && prefix != null)
            SpecialInputType.TogglePrefix
        else if (isLastItem || currentNumberEncoded == 0L)
            SpecialInputType.Nothing
        else if (separator != null && currentNumberEncoded != 0L)
            SpecialInputType.NextItem
        else
            SpecialInputType.Nothing
    }

    val result: InputResult = InputResult(
        prefixSet = prefixSet,
        items = (previousItems + (if (currentNumberEncoded > 0) listOf(currentNumberEncoded) else emptyList()))
            .map { decodeNumber(it) }
    )

    fun handleDigit(digit: Int): InputState {
        if (digit < 0 || digit >= BASE) throw IllegalArgumentException()

        val newValue = currentNumberEncoded * (BASE + 1) + digit + 1
        val oldValue = (newValue - 1 - digit) / (BASE + 1)

        if (newValue < 0 || oldValue != currentNumberEncoded) return this

        return copy(currentNumberEncoded = newValue)
    }

    fun handleSpecial(): InputState = when (specialInputType) {
        SpecialInputType.Nothing -> throw IllegalStateException()
        SpecialInputType.TogglePrefix -> copy(prefixSet = !prefixSet)
        SpecialInputType.NextItem -> copy(
                previousItems = previousItems + listOf(currentNumberEncoded),
                currentNumberEncoded = 0
        )
    }

    fun handleDelete(): InputState {
        return if (currentNumberEncoded > 0) {
            copy(
                    currentNumberEncoded = currentNumberEncoded / (BASE + 1)
            )
        } else if (previousItems.isNotEmpty()) {
            val previousNumber = previousItems.get(previousItems.size - 1)

            copy(
                    previousItems = previousItems.subList(0, previousItems.size - 1),
                    currentNumberEncoded = previousNumber
            )
        } else if (prefixSet) {
            copy(prefixSet = false)
        } else {
            this
        }
    }

    override fun toString(): String {
        val prefixString = if (prefixSet) prefix!! else ""
        val previousItemsString = if (previousItems.isEmpty()) "" else { previousItems.joinToString(separator = separator!!) { stringifyEncodedNumber(it) } + separator }
        val thisItemString = stringifyEncodedNumber(currentNumberEncoded)

        return prefixString + previousItemsString + thisItemString
    }
}

enum class SpecialInputType {
    Nothing,
    TogglePrefix,
    NextItem
}

data class InputResult(val prefixSet: Boolean, val items: List<Long>)