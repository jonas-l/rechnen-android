/*
 * Calculate Android Copyright <C> 2022 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.ui.training

import androidx.lifecycle.*
import com.rechnen.app.data.Database
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.launch
import kotlinx.coroutines.plus

class TrainingModel: ViewModel() {
    private val input = Channel<TrainingInputEvent>(Channel.CONFLATED)
    private val job = Job()
    private val scope = GlobalScope + job
    private val stateInternal = MutableLiveData<TrainingState>()
    private val currentNumberInputInternal = MutableLiveData<InputState>()
    private var didInit = false

    val state: LiveData<State> = object: MediatorLiveData<State>() {
        private var state: TrainingState? = null
        private var input: InputState? = null

        fun update() {
            val state = state; val input = input

            if (state != null && input != null) {
                value = State(state, input)
            }
        }

        init {
            addSource(stateInternal) { state = it; update() }
            addSource(currentNumberInputInternal) { input = it; update() }
        }
    }

    fun init(
            database: Database,
            userId: Int,
            mode: TrainingMode
    ) {
        if (didInit) {
            return
        }

        didInit = true

        scope.launch {
            TrainingUtil.start(
                    input = input,
                    database = database,
                    userId = userId,
                    scope = scope,
                    mode = mode
            ).consumeEach {
                stateInternal.postValue(it)

                if (it is TrainingState.ShowTask && it.clearInput) {
                    currentNumberInputInternal.postValue(InputState.newInstance(
                            prefix = it.task.prefix,
                            separator = it.task.separator,
                            maxListLength = it.task.maxListLength
                    ))
                }
            }
        }
    }

    fun handleNumberButton(digit: Int) {
        currentNumberInputInternal.value = currentNumberInputInternal.value?.handleDigit(digit)
    }

    fun handleDeleteButton() {
        currentNumberInputInternal.value = currentNumberInputInternal.value?.handleDelete()
    }

    fun handleConfirmButton() {
        val current = currentNumberInputInternal.value

        if (current == null)
            input.offer(
                TrainingInputEvent.Result(
                    values = emptyList(),
                    prefixEnabled = false
                )
            )
        else if (current.specialInputType == SpecialInputType.Nothing)
            input.offer(
                TrainingInputEvent.Result(
                    values = current.result.items,
                    prefixEnabled = current.prefixSet
                )
            )
        else
            currentNumberInputInternal.value = current.handleSpecial()
    }

    fun handleContinueButton() {
        input.offer(TrainingInputEvent.Continue)
    }

    override fun onCleared() {
        super.onCleared()

        job.cancel()
    }

    data class State(val traning: TrainingState, val input: InputState)
}