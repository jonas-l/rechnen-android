/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.ui.user

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.rechnen.app.R
import com.rechnen.app.data.AppDatabase
import com.rechnen.app.databinding.ViewUserStatisticBinding
import com.rechnen.app.databinding.ViewUserStatisticItemBinding
import com.rechnen.app.ui.tasktype.TaskTypeInfo
import com.rechnen.app.ui.util.Time

class ViewUserStatisticDialogFragment: BottomSheetDialogFragment() {
    companion object {
        private const val DIALOG_TAG = "ViewUserStatisticDialogFragment"
        private const val USER_ID = "userId"

        fun newInstance(userId: Int) = ViewUserStatisticDialogFragment().apply {
            arguments = Bundle().apply {
                putInt(USER_ID, userId)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = ViewUserStatisticBinding.inflate(inflater, container, false)
        val userId = arguments!!.getInt(USER_ID)
        val database = AppDatabase.with(context!!)

        database.user().getUserByIdLive(userId).observe(viewLifecycleOwner, Observer {
            it ?: kotlin.run { dismissAllowingStateLoss(); return@Observer }

            binding.username = it.name
            binding.playDuration = if (it.playTime == 0L) null else Time.formatTime(it.playTime, context!!)
            binding.solvedTasksString = resources.getQuantityString(R.plurals.statistic_solved_tasks, it.solvedTasks.toInt(), it.solvedTasks.toInt())
        })

        database.statisticTaskResult().queryAggregatedLive(userId).observe(viewLifecycleOwner, Observer { items ->
            binding.showRecentlyStatsHeader = items.isNotEmpty()

            binding.items.removeAllViews()

            items.forEach { item ->
                ViewUserStatisticItemBinding.inflate(inflater, binding.items, true).let { binding ->
                    binding.operationName = getString(TaskTypeInfo.get(item.type).labelResourceId)
                    binding.correctPercent = item.correct * 100 / item.total
                }
            }
        })

        return binding.root
    }

    fun show(fragmentManager: FragmentManager) = show(fragmentManager, DIALOG_TAG)
}