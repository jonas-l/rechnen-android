/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.data.modelparts

import android.util.JsonReader
import android.util.JsonWriter

data class FactorizationConfig(
        val enable: Boolean = false,
        val smallestNumber: Int = 4,
        val biggestNumber: Int = 150
) {
    companion object {
        private const val ENABLE = "enable"
        private const val SMALLEST_NUMBER = "smallestNumber"
        private const val BIGGEST_NUMBER = "biggestNumber"

        const val MIN_NUMBER = 1
        const val MAX_NUMBER = 65536

        fun parse(reader: JsonReader): FactorizationConfig {
            var enable: Boolean? = null
            var smallestNumber: Int? = null
            var biggestNumber: Int? = null

            reader.beginObject()
            while (reader.hasNext()) {
                when (reader.nextName()) {
                    ENABLE -> enable = reader.nextBoolean()
                    SMALLEST_NUMBER -> smallestNumber = reader.nextInt()
                    BIGGEST_NUMBER -> biggestNumber = reader.nextInt()
                    else -> reader.skipValue()
                }
            }
            reader.endObject()

            return FactorizationConfig(
                    enable = enable!!,
                    smallestNumber = smallestNumber!!,
                    biggestNumber = biggestNumber!!
            )
        }
    }

    init {
        if (smallestNumber < MIN_NUMBER || biggestNumber > MAX_NUMBER || smallestNumber > biggestNumber) {
            throw IllegalArgumentException()
        }
    }

    fun serialize(writer: JsonWriter) {
        writer.beginObject()

        writer.name(ENABLE).value(enable)
        writer.name(SMALLEST_NUMBER).value(smallestNumber)
        writer.name(BIGGEST_NUMBER).value(biggestNumber)

        writer.endObject()
    }
}