/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.rechnen.app.data.model.AggregatedStatisticTaskResult
import com.rechnen.app.data.model.StatisticTaskResult

@Dao
interface StatisticTaskResultDao {
    @Insert
    fun insertItemsSync(items: List<StatisticTaskResult>)

    @Query("DELETE FROM statistic_task_result WHERE user_id = :userId AND task_index < :lastIndexToKeep")
    fun cleanup(userId: Int, lastIndexToKeep: Long)

    @Query("DELETE FROM statistic_task_result WHERE user_id = :userId")
    fun deleteStatisticTaskResultsForUserSync(userId: Int)

    @Query("SELECT type, COUNT(*) AS total, SUM(CASE correct WHEN 0 THEN 0 ELSE 1 END) AS correct FROM statistic_task_result WHERE user_id = :userId GROUP BY type")
    fun queryAggregatedLive(userId: Int): LiveData<List<AggregatedStatisticTaskResult>>

    @Query("SELECT * FROM statistic_task_result WHERE user_id = :userId")
    suspend fun getItemsByUserIdCoroutine(userId: Int): List<StatisticTaskResult>
}