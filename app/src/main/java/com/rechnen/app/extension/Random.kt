/*
 * Calculate Android Copyright <C> 2020 Jonas Lochmann
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/
 */
package com.rechnen.app.extension

import java.util.*

val globalRandom = Random()

fun Random.nextIntInRange(min: Int, max: Int) = if (min >= max) {
    min
} else {
    this.nextInt(max - min + 1) + min
}

fun Random.nextIntWithDigits(numOfDigits: Int) = this.nextIntInRange(
	// avoid trivial numbers (1, 10, etc)
        min = Math.pow(10.0, (numOfDigits - 1).toDouble()).toInt() + 1,
        max = Math.pow(10.0, (numOfDigits ).toDouble()).toInt() - 1
)